<?php

/**
 * This is the model class for table "buku".
 *
 * The followings are the available columns in table 'buku':
 * @property integer $id
 * @property string $nama
 * @property string $tahun_terbit
 * @property integer $id_penulis
 * @property integer $id_penerbit
 * @property integer $id_kategori
 * @property string $sinopsis
 * @property string $sampul
 * @property string $berkas
 * @property string $harga
 */
class Buku extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'buku';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama, tahun_terbit, id_penulis, id_penerbit, id_kategori, sampul, berkas', 'required'),
			array('id_penulis, id_penerbit, id_kategori', 'numerical', 'integerOnly'=>true),
			array('nama, sinopsis, sampul, berkas, harga', 'length', 'max'=>255),
			array('tahun_terbit', 'length', 'max'=>4),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nama, tahun_terbit, id_penulis, id_penerbit, id_kategori, sinopsis, sampul, berkas, harga', 'safe', 'on'=>'search'),
			array('tahun_terbit', 'match', 'pattern' => '/^[0-9]\w*$/i','message' => 'Max 4 Number and Enter (0-9)'),
			//array('sampul', 'file', 'types'=>'jpg,gif,png', 'maxSize' => 1024 * 1024 * 10),
			//array('berkas', 'file', 'types'=>'doc,docx,xls,xlsx,pdf,ppt'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idpenulis' => array(self::BELONGS_TO, 'Penulis', 'id_penulis'),
			'idpenerbit' => array(self::BELONGS_TO, 'Penerbit', 'id_penerbit'),
			'idkategori' => array(self::BELONGS_TO, 'Kategori', 'id_kategori'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama' => 'Nama',
			'tahun_terbit' => 'Tahun Terbit',
			'id_penulis' => 'Penulis',
			'id_penerbit' => 'Penerbit',
			'id_kategori' => 'Kategori',
			'sinopsis' => 'Sinopsis',
			'sampul' => 'Sampul',
			'berkas' => 'Berkas',
			'harga' => 'Harga',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('tahun_terbit',$this->tahun_terbit,true);
		$criteria->compare('id_penulis',$this->id_penulis);
		$criteria->compare('id_penerbit',$this->id_penerbit);
		$criteria->compare('id_kategori',$this->id_kategori);
		$criteria->compare('sinopsis',$this->sinopsis,true);
		$criteria->compare('sampul',$this->sampul,true);
		$criteria->compare('berkas',$this->berkas,true);
		$criteria->compare('harga',$this->harga,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Buku the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
