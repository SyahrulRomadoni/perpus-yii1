<?php

class BukuController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'excel'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Buku;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Buku']))
		{
			$model->attributes=$_POST['Buku'];

			$sampul = CUploadedFile::getInstance($model, 'sampul');
			$berkas = CUploadedFile::getInstance($model, 'berkas');
			$sampul->saveAS(yii::getPathOfAlias('webroot').'/uploads/cover/'.time().'_'.$sampul->name);
			$berkas->saveAS(yii::getPathOfAlias('webroot').'/uploads/documents/'.time().'_'.$berkas->name);
			$model->sampul = time().'_'.$sampul->name;
			$model->berkas = time().'_'.$berkas->name;

			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		$sampul_lama = $model->sampul;
		$berkas_lama = $model->berkas;

		if(isset($_POST['Buku']))
		{
			$model->attributes=$_POST['Buku'];

			$sampul = CUploadedFile::getInstance($model, 'sampul');
			$berkas = CUploadedFile::getInstance($model, 'berkas');

			if ($sampul !== null) {
                unlink(yii::getPathOfAlias('webroot').'/uploads/cover/'.$sampul_lama);
                $model->sampul = time().'_'.$sampul->name;
                $sampul->saveAS(yii::getPathOfAlias('webroot').'/uploads/cover/'.time().'_'.$sampul->name);
            } else {
                $model->sampul = $sampul_lama;
            }

            if ($berkas !== null) {
                unlink(yii::getPathOfAlias('webroot').'/uploads/documents/'.$berkas_lama);
                $model->berkas = time().'_'.$berkas->name;
                $berkas->saveAS(yii::getPathOfAlias('webroot').'/uploads/documents/'.time().'_'.$berkas->name);
            } else {
                $model->berkas = $berkas_lama;
            }

			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		unlink(yii::getPathOfAlias('webroot').'/uploads/cover/'.$model->sampul);
		unlink(yii::getPathOfAlias('webroot').'/uploads/documents/'.$model->berkas);
		$model->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Buku');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Buku('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Buku']))
			$model->attributes=$_GET['Buku'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Buku the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Buku::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Buku $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='buku-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public static function actionExcel()
    {
    	require_once('protected/extensions/PHPExcel.php');
        $excel = new \PHPExcel();
        $excel->setActiveSheetIndex(0);
        $sheet = $excel->getActiveSheet();

        $sheet->getColumnDimension('B')->setWidth(10);
        $sheet->getColumnDimension('C')->setWidth(20);
        $sheet->getColumnDimension('D')->setWidth(20);
        $sheet->getColumnDimension('E')->setWidth(20);
        $sheet->getColumnDimension('F')->setWidth(20);
        $sheet->getColumnDimension('G')->setWidth(20);
        $sheet->getColumnDimension('H')->setWidth(20);
        $sheet->getColumnDimension('I')->setWidth(20);
        $sheet->getColumnDimension('J')->setWidth(20);
        $sheet->getColumnDimension('K')->setWidth(20);

        $sheet->setCellValue('B3', strtoupper('No'));
        $sheet->setCellValue('C3', strtoupper('Nama'));
        $sheet->setCellValue('D3', strtoupper('Tahun Terbit'));
        $sheet->setCellValue('E3', strtoupper('Penulis'));
        $sheet->setCellValue('F3', strtoupper('Penerbit'));
        $sheet->setCellValue('G3', strtoupper('Kategori'));
        $sheet->setCellValue('H3', strtoupper('Sinopsis'));
        $sheet->setCellValue('I3', strtoupper('Sampul'));
        $sheet->setCellValue('J3', strtoupper('Berkas'));
        $sheet->setCellValue('K3', strtoupper('Harga'));

        $excel->getActiveSheet()->setCellValue('B2', 'Daftar Buku');
        $excel->getActiveSheet()->mergeCells('B2:K2');

        $row = 3;
        $i=1;

        $model = new Buku();

        foreach($model->findAll() as $buku){
            $row++;
            $sheet->setCellValue('B' . $row, $i);
            $sheet->setCellValue('C' . $row, $buku->nama);
            $sheet->setCellValue('D' . $row, $buku->tahun_terbit);
            $sheet->setCellValue('E' . $row, $buku->id_penulis);
            $sheet->setCellValue('F' . $row, $buku->id_penerbit);
            $sheet->setCellValue('G' . $row, $buku->id_kategori);
            $sheet->setCellValue('H' . $row, $buku->sinopsis);
            $sheet->setCellValue('I' . $row, $buku->sampul);
            $sheet->setCellValue('J' . $row, $buku->berkas);
            $sheet->setCellValue('K' . $row, $buku->harga);
            $i++;
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.time().'_SR.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $objWriter->save('php://output');
        exit();
    }
}
