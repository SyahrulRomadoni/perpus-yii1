<?php
/* @var $this UserController */
/* @var $data User */
?>

<div class="view">

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />
	*/ ?>

	<b><?php echo CHtml::encode($data->getAttributeLabel('username')); ?>:</b>
	<?php echo CHtml::encode($data->username); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('password')); ?>:</b>
	<?php echo CHtml::encode($data->password); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_anggota')); ?>:</b>
	<?php //echo CHtml::encode($data->id_anggota);
		if ($data->id_anggota == 0) {
            echo "Bukan Anggota";
        } else {
        	echo $data->id_anggota;
        }
	?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_petugas')); ?>:</b>
	<?php //echo CHtml::encode($data->id_petugas);
		if ($data->id_petugas == 0) {
            echo "Bukan Petugas";
        } else {
        	echo $data->id_petugas;
        }
	?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_user_role')); ?>:</b>
	<?php //echo CHtml::encode($data->id_user_role); 
		if ($data->id_user_role == 1) {
            echo "Admin";
        }
        if ($data->id_user_role == 2) {
            echo "Petugas";
        }
        if ($data->id_user_role == 3) {
            echo "Anggota";
        }
	?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php //echo CHtml::encode($data->status); 
		if ($data->status == 0) {
            echo "Tidak Aktif";
        }
        if ($data->status == 1) {
            echo "Aktif";
        }
	?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('token')); ?>:</b>
	<?php echo CHtml::encode($data->token); ?>
	<br />

	*/ ?>

</div>