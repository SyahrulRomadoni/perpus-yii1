<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_anggota'); ?>
		<?php echo $form->textField($model,'id_anggota'); ?>
		<?php echo $form->error($model,'id_anggota'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_petugas'); ?>
		<?php echo $form->textField($model,'id_petugas'); ?>
		<?php echo $form->error($model,'id_petugas'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_user_role'); ?>
		<?php echo $form->dropDownList($model,'id_user_role', CHtml::ListData(UserRole::model()->findAll(),'id','nama')); ?>
		<?php echo $form->error($model,'id_user_role'); ?>
	</div>

	<?php /*
	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>
	*/ ?>

	<?php /*
	<div class="row">
		<?php echo $form->labelEx($model,'token'); ?>
		<?php echo $form->textField($model,'token',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'token'); ?>
	</div>
	*/ ?>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->