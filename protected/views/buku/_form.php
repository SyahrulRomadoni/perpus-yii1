<?php
/* @var $this BukuController */
/* @var $model Buku */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'buku-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data'
	)
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nama'); ?>
		<?php echo $form->textField($model,'nama',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'nama'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tahun_terbit'); ?>
		<?php echo $form->textField($model,'tahun_terbit',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'tahun_terbit'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_penulis'); ?>
		<?php echo $form->dropDownList($model,'id_penulis', CHtml::ListData(Penulis::model()->findAll(),'id','nama')); ?>
		<?php echo $form->error($model,'id_penulis'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_penerbit'); ?>
		<?php echo $form->dropDownList($model,'id_penerbit', CHtml::ListData(Penerbit::model()->findAll(),'id','nama')); ?>
		<?php echo $form->error($model,'id_penerbit'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_kategori'); ?>
		<?php echo $form->dropDownList($model,'id_kategori', CHtml::ListData(Kategori::model()->findAll(),'id','nama')); ?>
		<?php echo $form->error($model,'id_kategori'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sinopsis'); ?>
		<?php echo $form->textField($model,'sinopsis',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'sinopsis'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sampul'); ?>
		<?php echo $form->fileField($model,'sampul',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'sampul'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'berkas'); ?>
		<?php echo $form->fileField($model,'berkas',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'berkas'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'harga'); ?>
		<?php echo $form->textField($model,'harga',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'harga'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->