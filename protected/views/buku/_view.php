<?php
/* @var $this BukuController */
/* @var $data Buku */
?>

<div class="view">

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />
	*/ ?>

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tahun_terbit')); ?>:</b>
	<?php echo CHtml::encode($data->tahun_terbit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_penulis')); ?>:</b>
	<?php echo CHtml::encode($data->id_penulis); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_penerbit')); ?>:</b>
	<?php echo CHtml::encode($data->id_penerbit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_kategori')); ?>:</b>
	<?php echo CHtml::encode($data->id_kategori); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sinopsis')); ?>:</b>
	<?php echo CHtml::encode($data->sinopsis); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('sampul')); ?>:</b>
	<?php echo CHtml::encode($data->sampul); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('berkas')); ?>:</b>
	<?php echo CHtml::encode($data->berkas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('harga')); ?>:</b>
	<?php echo CHtml::encode($data->harga); ?>
	<br />

	*/ ?>

</div>