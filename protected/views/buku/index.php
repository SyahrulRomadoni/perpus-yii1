<?php
/* @var $this BukuController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Bukus',
);

$this->menu=array(
	array('label'=>'Create Buku', 'url'=>array('create')),
	array('label'=>'Manage Buku', 'url'=>array('admin')),
);
?>

<h1>Bukus</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
