<?php
/* @var $this BukuController */
/* @var $model Buku */

$this->breadcrumbs=array(
	'Bukus'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Buku', 'url'=>array('index')),
	array('label'=>'Create Buku', 'url'=>array('create')),
	array('label'=>'View Buku', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Buku', 'url'=>array('admin')),
);
?>

<h1>Update Buku <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>