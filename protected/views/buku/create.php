<?php
/* @var $this BukuController */
/* @var $model Buku */

$this->breadcrumbs=array(
	'Bukus'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Buku', 'url'=>array('index')),
	array('label'=>'Manage Buku', 'url'=>array('admin')),
);
?>

<h1>Create Buku</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>