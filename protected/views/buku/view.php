<?php
/* @var $this BukuController */
/* @var $model Buku */

$this->breadcrumbs=array(
	'Bukus'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Buku', 'url'=>array('index')),
	array('label'=>'Create Buku', 'url'=>array('create')),
	array('label'=>'Update Buku', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Buku', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Buku', 'url'=>array('admin')),
);
?>

<h1>View Buku #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id',
		'nama',
		'tahun_terbit',
		'idpenulis.nama',
		'idpenerbit.nama',
		'idkategori.nama',
		'sinopsis',
		'sampul',
		'berkas',
		'harga',
	),
)); ?>
