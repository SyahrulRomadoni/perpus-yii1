<?php
/* @var $this PenulisController */
/* @var $model Penulis */

$this->breadcrumbs=array(
	'Penulises'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Penulis', 'url'=>array('index')),
	array('label'=>'Create Penulis', 'url'=>array('create')),
	array('label'=>'View Penulis', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Penulis', 'url'=>array('admin')),
);
?>

<h1>Update Penulis <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>