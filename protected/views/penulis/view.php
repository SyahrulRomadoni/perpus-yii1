<?php
/* @var $this PenulisController */
/* @var $model Penulis */

$this->breadcrumbs=array(
	'Penulises'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Penulis', 'url'=>array('index')),
	array('label'=>'Create Penulis', 'url'=>array('create')),
	array('label'=>'Update Penulis', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Penulis', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Penulis', 'url'=>array('admin')),
);
?>

<h1>View Penulis #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id',
		'nama',
		'alamat',
		'telepon',
		'email',
		'foto',
	),
)); ?>
