<?php
/* @var $this PenulisController */
/* @var $model Penulis */

$this->breadcrumbs=array(
	'Penulises'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Penulis', 'url'=>array('index')),
	array('label'=>'Manage Penulis', 'url'=>array('admin')),
);
?>

<h1>Create Penulis</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>